# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_nereid_webshop_gift_card import suite

__all__ = ['suite']
