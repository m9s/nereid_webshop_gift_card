# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class NereidWebshopGiftCardTestCase(ModuleTestCase):
    'Test Nereid Webshop Gift Card module'
    module = 'nereid_webshop_gift_card'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            NereidWebshopGiftCardTestCase))
    return suite
