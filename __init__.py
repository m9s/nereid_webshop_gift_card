# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from checkout import *
from website import *


def register():
    Pool.register(
        Checkout,
        Website,
        module='nereid_webshop_gift_card', type_='model')
