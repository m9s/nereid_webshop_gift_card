# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

__all__ = ['Website']


class Website:
    __metaclass__ = PoolMeta
    __name__ = 'nereid.website'

    accept_gift_card = fields.Boolean('Accept Gift Card')
